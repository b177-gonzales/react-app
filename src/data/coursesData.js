const coursesData = [
	{
		id: "wdc001",
		name: "PHP - Laravel",
		description: " Lorem",
		price: 45000,
		onOffer: true
	},
	{
		id: "wdc002",
		name: "Python - Django",
		description: " Lorem",
		price: 50000,
		onOffer: true
	},
	{
		id: "wdc003",
		name: "Java - Springboot",
		description: " Lorem",
		price: 55000,
		onOffer: true
	}
];

export default coursesData;
